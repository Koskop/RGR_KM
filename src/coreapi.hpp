#ifndef COREAPI_HPP
#define COREAPI_HPP

#include <QObject>
#include <QString>
#include <QVariantList>
#include <QVector>

/**
 * @brief The CoreApi class
 * Store and process all data from UI
 */
class CoreApi : public QObject {
  Q_OBJECT

  Q_PROPERTY(qint64 factorsCount READ getFactorsCount WRITE setFactorsCount
                 NOTIFY factorsCountChanged)

  Q_PROPERTY(qint64 fragmentationCount READ getFragmentationCount WRITE
                 setFragmentationCount NOTIFY fragmentationCountChanged)

  Q_PROPERTY(
      qint64 mNumber READ getMNumber WRITE setMNumber NOTIFY mNumberChanged)

  Q_PROPERTY(QString resultText READ getResultText WRITE setResultText NOTIFY
                 resultTextChanged)

 public:
  explicit CoreApi(QObject *parent = nullptr);

  Q_INVOKABLE void processOperations();

  Q_INVOKABLE void calculateN();

  Q_INVOKABLE void calculateAll();

  Q_INVOKABLE void addEquation(qint64 uID, QVariantList list);

  Q_INVOKABLE void removeEquation(qint64 uID);

  // Getters

  qint64 getFactorsCount();

  qint64 getFragmentationCount();

  qint64 getMNumber();

  Q_INVOKABLE QString getListOfEquationsInStringRepresentation();

  QVector<int> getXBase();

  QVector<int> getXDeltaBase();

  QVector<QVector<double>> getYs();

  QString getResultText();

  // Setters

  void setFactorsCount(qint64 factorCount = 0);

  void setFragmentationCount(qint64 fragmentationCount = 0);

  void setMNumber(qint64 m = 0);

  Q_INVOKABLE void setXBase(QVariantList list);

  Q_INVOKABLE void setXDeltaBase(QVariantList list);

  Q_INVOKABLE void setYs(QVariantList list);

  void setResultText(QString text);

 signals:

  void factorsCountChanged(qint64 factorsCount);

  void fragmentationCountChanged(qint64 fragmentationCount);

  void nCountChanged(QString N);

  void mNumberChanged(qint64 m);

  void resultTextChanged(QString text);

  void error(QString err);

 public slots:

 private:
  qint64 m_factorsCount = 0;
  qint64 m_fragmentationCount = 0;
  qreal m_N = 0;
  qint64 m_m = 0;

  QString m_resText;

  QList<int> m_xBase;
  QList<int> m_xDeltaBase;
  QList<int> m_ys;

  QMap<qint64, QVariantList> m_equations;

  QVector<QVector<int>> getListOfEquations();
};

#endif  // COREAPI_HPP
