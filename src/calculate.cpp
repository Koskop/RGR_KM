#include "calculate.hpp"

Calculate::Calculate(int factorsC, int fragmentationC) {
  this->setFactorsCount(factorsC);
  this->setFragmentationCount(fragmentationC);
  this->N = int(pow(2.0, factorsC - fragmentationC));

  qInfo() << "Create Calculate object";
}

void Calculate::generatePlanningMatrix() {
  QVector<QVector<int>> finalMatrix;
  QVector<QVector<QString>> matrixOfCombination;
  QVector<QVector<QString>> dimensionlessMatrixOfCombinations;

  // generate new rows
  for (int j = 0; j < this->getN(); j++) {
    QVector<int> row;
    int u = j;
    QString binary = QString::fromStdString(std::bitset<32>(u).to_string());

    row.push_back(1);
    for (int var = 0;
         var < this->getFactorsCount() - this->getFragmentationCount(); ++var) {
      if (binary[binary.length() - var - 1].digitValue() == 0) {
        row.push_back(-1);
      } else {
        row.push_back(1);
      }
    }
    finalMatrix.push_back(row);
  }

  for (int i = 0; i < finalMatrix.length(); ++i) {
    QVector<QString> tmpCombination;
    QVector<QString> tmpDMOC;
    for (int j = 0; j < finalMatrix[i].length(); ++j) {
      // matrixOfCombination
      if (j != 0) {
        if (finalMatrix[i][j] == 1) {
          tmpCombination.push_back(QString::number(
              this->getXBase()[j - 1] + this->getXDeltaBase()[j - 1]));
        } else {
          tmpCombination.push_back(QString::number(
              this->getXBase()[j - 1] - this->getXDeltaBase()[j - 1]));
        }
      }

      // dimensionlessMatrixOfCombinations
      if (j == 0) {
        tmpDMOC.push_back(QString::number(finalMatrix[i][j]));
      } else {
        if (finalMatrix[i][j] == 1) {
          tmpDMOC.push_back(
              "=(" +
              QString::number(this->getXBase()[j - 1] +
                              this->getXDeltaBase()[j - 1]) +
              "-" + QString::number(this->getXBase()[j - 1]) + ")/" +
              QString::number(this->getXDeltaBase()[j - 1]) + "=1");
        } else {
          tmpDMOC.push_back(
              "=(" +
              QString::number(this->getXBase()[j - 1] -
                              this->getXDeltaBase()[j - 1]) +
              "-" + QString::number(this->getXBase()[j - 1]) + ")/" +
              QString::number(this->getXDeltaBase()[j - 1]) + "=-1");
        }
      }
    }
    matrixOfCombination.push_back(tmpCombination);
    dimensionlessMatrixOfCombinations.push_back(tmpDMOC);
  }

  // calculate fantom variables
  for (int i = 0; i < this->getFantomFactors().length(); i++) {
    for (int k = 0; k < finalMatrix.length(); k++) {
      int tmpI = 1;
      QString tmpS = "=";
      for (int j = 1; j < this->getFantomFactors()[i].length(); j++) {
        tmpI *= finalMatrix[k][getFantomFactors()[i][j]];
        tmpS += QString::number(finalMatrix[k][getFantomFactors()[i][j]]) + "*";
      }
      tmpS[tmpS.length() - 1] = '=';
      tmpS.push_back(QString::number(tmpI));
      finalMatrix[k].push_back(tmpI);
      dimensionlessMatrixOfCombinations[k].push_back(tmpS);
    }
  }

  this->m_matrixOfCombination = matrixOfCombination;
  this->m_dimensionlessMatrixOfCombinations = dimensionlessMatrixOfCombinations;
  this->m_finalMatrix = finalMatrix;

  // output data
  qInfo() << "Matrix Of Combination";
  for (auto a : matrixOfCombination) {
    qInfo() << a;
  }

  qInfo() << "Dimensionless Matrix Of Combinations";
  for (auto a : dimensionlessMatrixOfCombinations) {
    qInfo() << a;
  }

  qInfo() << "Final Matrix";
  for (auto a : finalMatrix) {
    qInfo() << a;
  }
}

void Calculate::generateEquation() {
  QString equation = "y = b0";
  for (int i = 0; i < this->getFactorsCount() - 1; i++) {
    equation += "+b" + QString::number(i + 1) + "*x" + QString::number(i + 1);
  }
  for (int i = 0; i < this->getFantomFactors().length(); i++) {
    QString tmpB = "+b";
    QString tmpX;
    for (int j = 1; j < this->getFantomFactors()[i].length(); j++) {
      tmpX += "*x" + QString::number(this->getFantomFactors()[i][j]);
      tmpB += QString::number(this->getFantomFactors()[i][j]);
    }
    equation += tmpB + tmpX;
  }

  this->m_equation = equation;

  // output data
  qInfo() << "Equation:";
  qInfo() << equation;
}

void Calculate::calcS() {
  ys.clear();

  for (auto i : this->getYArray()) {
    double tmp = 0;
    for (auto j : i) {
      tmp += j;
    }
    ys.push_back(tmp / i.size());
  }

  Ssq.clear();

  for (int i = 0; i < this->getYArray().size(); i++) {
    double tmp = 0;
    for (int j = 0; j < this->getYArray()[i].size(); j++) {
      tmp +=
          (this->getYArray()[i][j] - ys[i]) * (this->getYArray()[i][j] - ys[i]);
    }
    Ssq.push_back(tmp / (this->getYArray()[i].size() - 1));
  }
}

bool Calculate::calcG() {
  double maxSsq = -10000000000;
  for (auto i : Ssq) {
    if (i > maxSsq) {
      maxSsq = i;
    }
  }
  double sum = 0;
  for (auto i : Ssq) {
    sum += i;
  }

  this->G = maxSsq / sum;

  if (this->kohren[N - 1][this->getM() - 2] > this->G) {
    this->dispersion = sum / Ssq.size();
    return true;
  } else {
    return false;
  }
}

void Calculate::check5() {
  QVector<double> bj;

  for (int i = 0; i < this->getXArray()[0].size();
       i++) {  // витягти finalmatrix
    double tmp = 0;
    for (int j = 0; j < N; j++) {
      tmp += this->getXArray()[j][i] * this->ys[j];
    }
    bj.push_back(tmp / N);
  }

  Sbj = sqrt(dispersion / (this->getM() * N));

  // Student

  tbj.clear();
  for (auto i : bj) {
    tbj.push_back(fabs(i) / Sbj);
  }

  for (int i = 0; i < tbj.size(); i++) {
    if (tbj[i] < student[this->getN() * (getM() - 1)])
      ;
    { tbj[i] = 0; }
  }
  int l = 0;
  for (int i = 0; i < tbj.size(); i++) {
    if (tbj[i] != 0) {
      l++;
    }
  }
  double yl = bj[0];

  for (int i = 0; i < bj.size(); i++) {
    yl += bj[i] * 0;  // WARNING
  }

  double Sad = 0;
  double sum = 0;
  for (int i = 0; i < ys.size(); i++) {
    sum += ys[i] - yl;
  }
  Sad = sum * getM() / (getN() - l);

  double F = Sad / dispersion;

  if (F < fisher[getN() - l][getN() * (getM() - 1)]) {
    // out adekvtno
  } else {
    // ne adekvatno
  }
}

QString Calculate::generateOutputString() {
  // table1 head
  this->addToOutputString("<table><tr>");
  for (int i = 0; i < this->getFactorsCount() - 1; i++) {
    this->addToOutputString("<th>X" + QString::number(i + 1) + "</th>");
  }
  this->addToOutputString("</tr>");
  for (auto a : this->m_matrixOfCombination) {
    this->addToOutputString("<tr>");
    for (auto aa : a) {
      this->addToOutputString("<td>" + aa + "</td>");
    }
    this->addToOutputString("</tr>");
  }
  this->addToOutputString("</table>");

  // table2 head
  this->addToOutputString("<table><tr>");
  for (int i = 0; i < this->getFactorsCount() + this->getFragmentationCount();
       i++) {
    this->addToOutputString("<th>X" + QString::number(i + 1) + "</th>");
  }
  for (auto a : this->m_dimensionlessMatrixOfCombinations) {
    this->addToOutputString("<tr>");
    for (auto aa : a) {
      this->addToOutputString("<td>" + aa + "</td>");
    }
    this->addToOutputString("</tr>");
  }
  this->addToOutputString("</table>");

  //   table3 head
  this->addToOutputString("<table><tr>");
  for (int i = 0; i < this->getFactorsCount() + this->getFragmentationCount();
       i++) {
    this->addToOutputString("<th>X" + QString::number(i + 1) + "</th>");
  }
  for (auto a : this->m_finalMatrix) {
    this->addToOutputString("<tr>");
    for (auto aa : a) {
      this->addToOutputString("<td>" + QString::number(aa) + "</td>");
    }
    this->addToOutputString("</tr>");
  }
  this->addToOutputString("</table>");

  qInfo() << this->getOutputString();

  return this->getOutputString();
}

void Calculate::addToOutputString(QString value) {
  this->outputString += value;
}
void Calculate::clearOutputString() { this->outputString.clear(); }

void Calculate::setFactorsCount(int value) { this->factorsCount = value; }
void Calculate::setFragmentationCount(int value) {
  this->fragmentationCount = value;
}
void Calculate::setFantomFactors(QVector<int> value) {
  if (!fantomFactors.isEmpty()) {
    if (this->fantomFactors.first().first() < value.first()) {
      this->fantomFactors.push_back(value);
    } else {
      this->fantomFactors.push_front(value);
    }
  } else {
    this->fantomFactors.push_front(value);
  }
}
void Calculate::setXBase(QVector<int> base) { this->xBase = base; }
void Calculate::setXDeltaBase(QVector<int> deltaBase) {
  this->xDeltaBase = deltaBase;
}
void Calculate::setYArray(QVector<double> array) {
  this->yArray.push_back(array);
}
void Calculate::setM(int value) { this->M = value; }
void Calculate::setXArray(QVector<QVector<int>> value) { this->xArray = value; }
void Calculate::setOutputString(QString value) { this->outputString = value; };

int Calculate::getN() { return this->N; }
int Calculate::getM() { return this->M; }
int Calculate::getFactorsCount() { return this->factorsCount; }
int Calculate::getFragmentationCount() { return this->fragmentationCount; }
QVector<QVector<int>> Calculate::getFantomFactors() {
  return this->fantomFactors;
}
QVector<int> Calculate::getXBase() { return this->xBase; };
QVector<int> Calculate::getXDeltaBase() { return this->xDeltaBase; };
QVector<QVector<double>> Calculate::getYArray() { return this->yArray; }
QVector<QVector<int>> Calculate::getXArray() { return this->xArray; }
QString Calculate::getOutputString() { return this->outputString; }
