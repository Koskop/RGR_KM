#include "coreapi.hpp"

#include "calculate.hpp"

#include <QtDebug>

#include <cmath>
#include <exception>
// test
CoreApi::CoreApi(QObject* parent) : QObject(parent) {}

Q_INVOKABLE void CoreApi::processOperations() { m_equations.clear(); }

Q_INVOKABLE void CoreApi::calculateN() {
  qInfo() << Q_FUNC_INFO;

  qInfo() << "K = " << m_factorsCount;
  qInfo() << "P = " << m_fragmentationCount;
  qInfo() << "N = " << m_N;

  m_N = pow(2.0, m_factorsCount - m_fragmentationCount);

  emit nCountChanged("N = " + QString::number(m_N));
}

void CoreApi::setFactorsCount(qint64 factorCount) {
  m_factorsCount = factorCount;
  emit factorsCountChanged(m_factorsCount);
}

void CoreApi::setMNumber(qint64 m) {
  m_m = m;
  emit mNumberChanged(m_m);
}

Q_INVOKABLE void CoreApi::calculateAll() {
  try {
    // TODO
    Calculate* calc = new Calculate(int(this->getFactorsCount()),
                                    int(this->getFragmentationCount()));
    qInfo() << getXBase();
    calc->setXBase(this->getXBase());
    //    calc->setXBase({50, 50, 50});
    qInfo() << calc->getXBase();
    qInfo() << this->getXDeltaBase();
    calc->setXDeltaBase(this->getXDeltaBase());
    //    calc->setXDeltaBase({20, 20, 20, 20});
    qInfo() << calc->getXDeltaBase();

    qInfo() << "#1";

    auto temp1 = this->getListOfEquations();

    for (auto a : temp1) {
      calc->setFantomFactors(a);
    }

    qInfo() << "#2";

    auto temp2 = this->getYs();
    for (auto a : temp2) {
      calc->setYArray(a);
    }

    qInfo() << "#3";

    calc->generatePlanningMatrix();

    qInfo() << "#4";

    calc->generateEquation();

    qInfo() << "#5";

    //  calc->calcS();
    //  calc->calcG();
    //  calc->check5();
    this->setResultText(calc->generateOutputString());

    qInfo() << "#6";

    delete calc;
  } catch (...) {
    emit error("Error !!!");
  }
}

qint64 CoreApi::getFactorsCount() { return m_factorsCount; }

qint64 CoreApi::getMNumber() { return m_m; }

void CoreApi::setFragmentationCount(qint64 fragmentationCount) {
  m_fragmentationCount = fragmentationCount;
  emit fragmentationCountChanged(m_fragmentationCount);
}

qint64 CoreApi::getFragmentationCount() { return m_fragmentationCount; }

QString CoreApi::getResultText() { return m_resText; }

void CoreApi::setResultText(QString text) {
  m_resText = text;
  emit resultTextChanged(m_resText);
}

Q_INVOKABLE void CoreApi::addEquation(qint64 uID, QVariantList list) {
  m_equations.insert(uID, list);

  qInfo() << "Debug" << list << uID;
}

Q_INVOKABLE void CoreApi::removeEquation(qint64 uID) {
  if (m_equations.contains(uID)) {
    m_equations.remove(uID);
  }
}

Q_INVOKABLE void CoreApi::setXBase(QVariantList list) {
  m_xBase.clear();

  qInfo() << list;

  for (auto i : list) m_xBase.append(i.toInt());
}

Q_INVOKABLE void CoreApi::setXDeltaBase(QVariantList list) {
  m_xDeltaBase.clear();

  qInfo() << list;

  for (auto i : list) m_xDeltaBase.append(i.toInt());
}

Q_INVOKABLE void CoreApi::setYs(QVariantList list) {
  m_ys.clear();

  qInfo() << list;

  for (auto i : list) m_ys.append(i.toInt());
}

QVector<QVector<int>> CoreApi::getListOfEquations() {
  QVector<QVector<int>> ret;

  for (auto i : m_equations.keys()) {
    auto temp = m_equations[i];

    QVector<int> tempv;

    tempv.append(int(i));

    for (auto j : temp) tempv.append(j.toInt());

    ret.append(tempv);
  }

  return ret;
}

QString CoreApi::getListOfEquationsInStringRepresentation() {
  QString ret;

  for (auto i : m_equations.keys()) {
    auto temp = m_equations[i];

    temp.pop_front();

    ret += "X";
    ret += "<sub>" + QString::number(i) + "</sub> = ";

    for (auto j : temp) {
      ret += "X";
      ret += "<sub>" + QString::number(j.toInt()) + "</sub> * ";
    }
    ret.remove(ret.size() - 2, 2);
    ret += "<br>";
  }

  return ret;
}

QVector<int> CoreApi::getXBase() {
  QVector<int> res;

  for (int i = 0; i < m_xBase.size(); ++i) {
    res.append(m_xBase[i]);
  }

  return res;
}

QVector<int> CoreApi::getXDeltaBase() {
  QVector<int> res;

  for (int i = 0; i < m_xDeltaBase.size(); ++i) {
    res.append(m_xDeltaBase[i]);
  }

  return res;
}

QVector<QVector<double>> CoreApi::getYs() {
  QVector<QVector<double>> res;

  for (int i = 0; i < m_ys.size(); ++i) {
    QVector<double> row;

    row.append(m_ys[i]);

    if (i != 0 && i % int(m_N)) {
      res.append(row);
      row.clear();
    }
  }

  return res;
}
