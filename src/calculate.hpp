#ifndef CALCULATE_H
#define CALCULATE_H

#include <QString>
#include <QVector>
#include <QtDebug>
#include <bitset>
#include <cmath>

class Calculate {
 public:
  Calculate(int factorsC, int fragmentationC);

  // methodts
  void generatePlanningMatrix();
  void generateEquation();
  void calcS();
  bool calcG();
  void check5();

  QString generateOutputString();

  void addToOutputString(QString value);
  void clearOutputString();

  // setters
  void setFragmentationCount(int value);
  void setFactorsCount(int value);
  void setFantomFactors(QVector<int> value);  // example x4=x2*x1 - (4, 2, 1)
  void setXBase(QVector<int> base);
  void setXDeltaBase(QVector<int> deltaBase);
  void setYArray(QVector<double> array);
  void setM(int value);
  void setXArray(QVector<QVector<int>> value);
  void setOutputString(QString value);

  // getters
  int getFragmentationCount();
  int getFactorsCount();
  int getN();
  int getM();
  QVector<QVector<int>> getFantomFactors();
  QVector<int> getXBase();
  QVector<int> getXDeltaBase();
  QVector<QVector<double>> getYArray();
  QVector<QVector<int>> getXArray();
  QString getOutputString();

 private:
  // variables
  int fragmentationCount = 0;
  int factorsCount = 0;
  int N = 0;
  int M = 0;

  double G = 0;
  double dispersion = 0;
  double Sbj = 0;

  QVector<QVector<int>> fantomFactors;
  QVector<int> xBase;
  QVector<int> xDeltaBase;
  QVector<QVector<int>> xArray;
  QVector<QVector<double>> yArray;  // vector of y
  QVector<double> Ssq;
  QVector<double> tbj;
  QVector<double> ys;

  QString outputString;

  QVector<QVector<int>> m_finalMatrix;
  QVector<QVector<QString>> m_matrixOfCombination;
  QVector<QVector<QString>> m_dimensionlessMatrixOfCombinations;
  QString m_equation;

  // tebles
  QVector<QVector<double>> kohren = {{0.9985, 0.9750, 0.9392, 0.9057, 0.8772,
                                      0.8534, 0.8332, 0.8159, 0.8010, 0.7880},
                                     {0.9669, 0.8709, 0.7977, 0.7457, 0.7071,
                                      0.6771, 0.6530, 0.6333, 0.6167, 0.6025},
                                     {0.9065, 0.7879, 0.6841, 0.6287, 0.5895,
                                      0.5598, 0.5365, 0.5175, 0.5017, 0.4884},
                                     {0.8412, 0.6838, 0.5981, 0.5441, 0.5065,
                                      0.4783, 0.4564, 0.4387, 0.4241, 0.4118},
                                     {0.7808, 0.6161, 0.5321, 0.4803, 0.4447,
                                      0.4184, 0.3980, 0.3817, 0.3682, 0.3568},
                                     {0.7271, 0.5612, 0.4800, 0.4307, 0.3974,
                                      0.3726, 0.3535, 0.3384, 0.3259, 0.3154},
                                     {0.6798, 0.5157, 0.4377, 0.3910, 0.3595,
                                      0.3362, 0.3185, 0.3043, 0.2926, 0.2829},
                                     {0.6385, 0.4775, 0.4027, 0.3584, 0.3286,
                                      0.3067, 0.2901, 0.2768, 0.2659, 0.2568},
                                     {0.6020, 0.4450, 0.3733, 0.3311, 0.3029,
                                      0.2823, 0.2666, 0.2541, 0.2439, 0.2353}};

  QVector<double> student = {12.706, 4.303, 3.182, 2.776, 2.571, 2.447, 2.365,
                             2.306,  2.262, 2.228, 2.201, 2.179, 2.160, 2.145,
                             2.131,  2.120, 2.110, 2.101, 2.093, 2.086, 2.080,
                             2.074,  2.069, 2.064, 2.060, 2.056, 2.052, 2.048,
                             2.045,  2.042, 2.000, 1.980, 1.960};
  QVector<QVector<double>> fisher = {
      {161.45, 199.50, 215.71, 224.58, 230.16, 233.99, 236.77, 238.88, 240.54,
       241.88, 245.95},
      {18.51, 19.00, 19.16, 19.25, 19.30, 19.33, 19.35, 19.37, 19.38, 19.40,
       19.43},
      {10.13, 9.55, 9.28, 9.12, 9.01, 8.94, 8.89, 8.85, 8.81, 8.79, 8.70},
      {7.71, 6.94, 6.59, 6.39, 6.26, 6.16, 6.09, 6.04, 6.00, 5.96, 5.86},
      {6.61, 5.79, 5.41, 5.19, 5.05, 4.95, 4.88, 4.82, 4.77, 4.74, 4.62},
      {5.99, 5.14, 4.76, 4.53, 4.39, 4.28, 4.21, 4.15, 4.10, 4.06, 3.94},
      {5.59, 4.74, 4.35, 4.12, 3.97, 3.87, 3.79, 3.73, 3.68, 3.64, 3.51},
      {5.32, 4.46, 4.07, 3.84, 3.69, 3.58, 3.50, 3.44, 3.39, 3.35, 3.22},
      {5.12, 4.26, 3.86, 3.63, 3.48, 3.37, 3.29, 3.23, 3.18, 3.14, 3.01},
      {4.96, 4.10, 3.71, 3.48, 3.33, 3.22, 3.14, 3.07, 3.02, 2.98, 2.85},
      {4.84, 3.98, 3.59, 3.36, 3.20, 3.09, 3.01, 2.95, 2.90, 2.85, 2.72},
      {4.75, 3.89, 3.49, 3.26, 3.11, 3.00, 2.91, 2.85, 2.80, 2.75, 2.62},
      {4.67, 3.81, 3.41, 3.18, 3.03, 2.92, 2.83, 2.77, 2.71, 2.67, 2.53},
      {4.60, 3.74, 3.34, 3.11, 2.96, 2.85, 2.76, 2.70, 2.65, 2.60, 2.46},
      {4.54, 3.68, 3.29, 3.06, 2.90, 2.79, 2.71, 2.64, 2.59, 2.54, 2.40},
      {4.49, 3.63, 3.24, 3.01, 2.85, 2.74, 2.66, 2.59, 2.54, 2.49, 2.35},
      {4.45, 3.59, 3.20, 2.96, 2.81, 2.70, 2.61, 2.55, 2.49, 2.45, 2.31},
      {4.41, 3.55, 3.16, 2.93, 2.77, 2.66, 2.58, 2.51, 2.46, 2.41, 2.27},
      {4.38, 3.52, 3.13, 2.90, 2.74, 2.63, 2.54, 2.48, 2.42, 2.38, 2.23},
      {4.35, 3.49, 3.10, 2.87, 2.71, 2.60, 2.51, 2.45, 2.39, 2.35, 2.20}};
};

#endif  // CALCULATE_H
