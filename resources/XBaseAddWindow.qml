import QtQuick 2.4
import QtQuick.Controls 2.4

XBaseAddWindowForm {

    property int colNum: 0

    property variant tableArray

    property int lastIndex: 0

    property  int colWidth: item1.width / colNum


    Component.onCompleted:  {
        recreateTable()
    }

    function recreateTable() {
        lastIndex = 0
        var temp = []


        while(temp.length != colNum)
            temp.push(0)

        tableArray = temp
        listModel.clear()


        for(var i = 0; i < colNum; ++i) {
            var texttopaste = "X<sub>" + (i + 1) + "</sub>"
            listModel.append({isEnb: false, ts: texttopaste, li: -1})
        }

        for(var j = 0; j < colNum; ++j) {
            listModel.append({isEnb: true,ts:"", li: lastIndex})
            lastIndex++
        }

    }

    setBtn.onClicked:  {
        CoreApi.setXBase(tableArray)
    }

    ListModel {
        id: listModel
    }


    Component {
        id: delegatedComponent

        Item {
            Label {
                visible: !isEnb
                text: ts
                height: 40
                width: colWidth
                padding: 4
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment:  Qt.AlignVCenter

                background:  Rectangle {
                    color: "#666666"
                    border.color: "black"
                    border.width: 1
                }
            }

            TextField {
                visible: isEnb
                height: 40
                width: colWidth
                padding: 4
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment:  Qt.AlignVCenter

                onTextChanged:  {
                    if(isEnb) {
                        tableArray[li] = parseInt(text)
                    }
                }
            }
        }
    }
}
