import QtQuick 2.4
import QtQuick.Controls 2.3

Item {
    id: item1
    width: 420
    height: 120
    property alias flowXs: flowXs
    property alias removeBtn: removeBtn
    property alias addBtn: addBtn
    property alias index: index
    property alias spinBox: spinBox

    Label {
        id: label
        x: 16
        y: 17
        width: 75
        height: 31
        text: qsTr("X         =")
        font.pointSize: 26

        SpinBox {
            id: spinBox
            x: 90
            y: 46
            width: 131
            height: 40
            font.pointSize: 24
            value: 1
            to: 16
            from: 1
        }

        TextField {
            id: index
            x: 21
            y: 20
            width: 38
            height: 24
            text: qsTr("")
            padding: 1
            font.pointSize: 12
            validator: intvalidator
        }
    }

    Button {
        id: addBtn
        y: 17
        height: 40
        width: 120
        text: qsTr("Add")
        font.pointSize: 12
        anchors.right: parent.right
        anchors.rightMargin: 4
    }

    Label {
        id: label1
        x: 10
        y: 78
        width: 68
        height: 25
        text: qsTr("Xs count=")
        font.pointSize: 18
    }

    ListView {
        id: flowXs
        x: 106
        y: 17
        height: 40
        contentHeight: 40
        contentWidth: 40
        interactive: true
        cacheBuffer: 999
        snapMode: ListView.SnapToItem
        flickableDirection: Flickable.HorizontalFlick
        clip: true
        width: (40 + 2) * listModel.count
        model: listModel
        delegate: delegateItem
        spacing: 2
    }

    Button {
        id: removeBtn
        y: 63
        height: 40
        width: 120
        text: qsTr("Remove")
        font.pointSize: 12
        anchors.right: parent.right
        anchors.rightMargin: 4
    }
}
