import QtQuick 2.4
import QtQuick.Controls 2.4

Item {
    id: item1
    width: 400
    height: 400
    property alias setBtn: setBtn
    property alias toolBar: toolBar
    property alias table: table
    property alias item1: item1

    ToolBar {
        id: toolBar
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0

        Button {
            id: setBtn
            text: qsTr("Set")
        }
    }

    GridView {
        id: table
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        cellHeight: 40
        cellWidth: (parent.width / colNum)
        anchors.top: toolBar.bottom
        anchors.topMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        model: listModel
        delegate: delegatedComponent
    }
}


/*##^## Designer {
    D{i:1;anchors_width:360;anchors_x:49;anchors_y:54}
}
 ##^##*/
