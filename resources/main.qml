import QtQuick 2.9
import QtQuick.Window 2.2

Window {
    visible: true
    width: 900
    height: 800
    minimumWidth: 400
    title: qsTr("Розрахункова робота з предмету \"Комп'ютерне моделювання процесів і систем - 2\"")

    MainWindow
    {
        anchors.fill: parent
    }

}
