import QtQuick 2.4
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.0

Item {
    property alias processBtn: processBtn
    property alias nCount: nCount
    property alias fragmentationCount: fragmentationCount
    property alias factorCount: factorCount
    property alias dynemicXsItem: dynemicXsItem
    property alias mCount: mCount
    property alias openXBaseAdd: openXBaseAdd
    property alias openXDeltaBaseAdd: openXDeltaBaseAdd
    property alias openYAdd: openYAdd
    property alias equationText: equationText
    property alias calculateAll: calculateAll

    IntValidator {
        id: fieldValidator

        bottom: 0
        top: 4096
    }

    Flow {
        id: flowContainer
        anchors.fill: parent
        anchors.leftMargin: 2
        anchors.bottom: toolBar.top
        spacing: 2

        TextField {
            id: factorCount
            text: qsTr("")
            font.pointSize: 12
            placeholderText: "Введіть кількість факторів K ..."
            validator: fieldValidator
            selectByMouse: true
            width: flowContainer.width > 600 ? flowContainer.width / 4 - 2 : flowContainer.width - 2
        }

        TextField {
            id: fragmentationCount
            text: qsTr("")
            font.pointSize: 12
            placeholderText: "Введіть дробність P ..."
            validator: fieldValidator
            selectByMouse: true
            width: flowContainer.width > 600 ? flowContainer.width / 4 - 2 : flowContainer.width - 2
        }

        TextField {
            id: nCount
            text: qsTr("")
            placeholderText: "N = 2 ^ ( K + P )"
            font.pointSize: 12
            validator: fieldValidator
            selectByMouse: true
            width: flowContainer.width > 600 ? flowContainer.width / 4 - 2 : flowContainer.width - 2
        }

        Button {
            id: processBtn
            text: qsTr("Далі")
            font.pointSize: 12
            width: flowContainer.width > 600 ? flowContainer.width / 4 - 2 : flowContainer.width - 2
        }

        DynemicXsItem {
            id: dynemicXsItem
            visible: false
            width: flowContainer.width - 2
        }

        Text {
            id: equationText
            width: flowContainer.width - 2
            textFormat: Text.RichText
        }

        TextField {
            id: mCount
            visible: dynemicXsItem.visible
            text: qsTr("")
            font.pointSize: 12
            placeholderText: "Введіть m ..."
            validator: fieldValidator
            selectByMouse: true
            width: flowContainer.width - 2
        }

        Button {
            id: openXBaseAdd
            text: qsTr("X Base")
            visible: dynemicXsItem.visible
            font.pointSize: 12
            width: flowContainer.width > 600 ? flowContainer.width / 3 - 2 : flowContainer.width - 2
        }

        Button {
            id: openXDeltaBaseAdd
            text: qsTr("X Delta")
            visible: dynemicXsItem.visible
            font.pointSize: 12
            width: flowContainer.width > 600 ? flowContainer.width / 3 - 2 : flowContainer.width - 2
        }

        Button {
            id: openYAdd
            text: qsTr("Ys")
            visible: dynemicXsItem.visible
            font.pointSize: 12
            width: flowContainer.width > 600 ? flowContainer.width / 3 - 2 : flowContainer.width - 2
        }


        Button {
            id: calculateAll
            text: qsTr("Calculate")
            visible: dynemicXsItem.visible
            font.pointSize: 12
            width: flowContainer.width - 2
        }

        TextArea {
            id: textArea
            text: CoreApi.resultText
            font.weight: Font.Thin
            textFormat: Text.RichText
            width: flowContainer.width - 2
        }
    }

    ToolBar {
        id: toolBar
        position: ToolBar.Footer
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
    }
}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
