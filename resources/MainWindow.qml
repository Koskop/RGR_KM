import QtQuick 2.4
import QtQuick.Controls 2.4

MainWindowForm {
    mCount.onTextChanged: {
        CoreApi.mNumber = parseInt(mCount.text)
        CoreApi.calculateN()
    }

    factorCount.onTextChanged:  {
        CoreApi.factorsCount = parseInt(factorCount.text)
        CoreApi.calculateN()
    }

    fragmentationCount.onTextChanged: {

        CoreApi.fragmentationCount = parseInt(fragmentationCount.text)
        dynemicXsItem.maxIndex = parseInt(fragmentationCount.text)
        CoreApi.calculateN()
    }


    processBtn.onClicked: {
        if(checkFields())
        {
            CoreApi.calculateN()
            CoreApi.processOperations()
            dynemicXsItem.visible = true
            equationText.text = CoreApi.getListOfEquationsInStringRepresentation()
        }
    }

    calculateAll.onClicked:  {
        CoreApi.calculateAll()
    }

    dynemicXsItem.onAdded:  {
        equationText.text = CoreApi.getListOfEquationsInStringRepresentation()
    }

    nCount.onEditingFinished: {
        CoreApi.calculateN()
    }

    Connections {
        target: CoreApi

        onError: {
            errorPop.open()
            errorText.text = err
        }

    }

    function checkFields() {

        var status = true

        if(factorCount.text.length === 0)
        {
            factorCount.focus = true
            factorCount.text = "0"
            status = false;
        }

        if(fragmentationCount.text.length === 0)
        {
            fragmentationCount.focus = true
            fragmentationCount.text = "0"
            status = false;
        }

        return status
    }

    openXBaseAdd.onClicked:  {
        xBasePopup.open()
    }

    openXDeltaBaseAdd.onClicked:  {
        xDeltaBasePopup.open()
    }

    openYAdd.onClicked:  {
        ysAddPopup.open()
    }

    Connections {
        target: CoreApi

        onNCountChanged: {
            nCount.text = N
        }
    }

    Popup {
        id: xBasePopup
        padding: 0
        modal: true

        width: parent.width * 0.95
        height: parent.height * 0.95

        x: (parent.width - width) / 2
        y: (parent.height - height) / 2

        XBaseAddWindow {
            id: xBaseAddWindow
            anchors.fill: parent
            colNum: CoreApi.factorsCount - CoreApi.fragmentationCount
        }


        onOpened:  {
            xBaseAddWindow.recreateTable()
        }
    }

    Popup {
        id: xDeltaBasePopup
        padding: 0
        modal: true

        width: parent.width * 0.95
        height: parent.height * 0.95

        x: (parent.width - width) / 2
        y: (parent.height - height) / 2

        XDeltaBaseAddWindow {
            id: xDeltaAddWindow
            anchors.fill: parent
            colNum: CoreApi.factorsCount - CoreApi.fragmentationCount
        }

        onOpened:  {
            xDeltaAddWindow.recreateTable()
        }
    }

    Popup {
        id: ysAddPopup
        padding: 0
        modal: true

        width: parent.width * 0.95
        height: parent.height * 0.95

        x: (parent.width - width) / 2
        y: (parent.height - height) / 2

        YsAddWindow {
            id: ysAddWindow
            anchors.fill: parent
            colNum: CoreApi.mNumber
            rowCount: Math.pow(2,CoreApi.factorsCount - CoreApi.fragmentationCount)
        }

        onOpened:  {
            ysAddWindow.recreateTable()
        }
    }

    Popup {
        id: errorPop

        width: parent.width * 0.6
        height: parent.height * 0.6

        Text {
            id: errorText
            text: ""
            horizontalAlignment: Qt.AlignHCenter
            verticalAlignment:  Qt.AlignVCenter
        }
    }
}
