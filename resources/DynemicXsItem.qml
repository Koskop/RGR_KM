import QtQuick 2.4
import QtQuick.Controls 2.4

DynemicXsItemForm {

    property int maxIndex: 0
    property string eql: ""

    property int lastX: 0
    property int lastY: 0

    property variant valuesOfXs
    property int lastIndex: 0

    signal added()



    spinBox.onValueChanged: {
        creator()
    }

    function creator() {
        while(spinBox.value > listModel.count) {
            listModel.append({lx: lastX, ly: lastY, li: lastIndex})
            lastX += 42
            lastY -= 2
            lastIndex++

            valuesOfXs.push(0)
        }

        while(spinBox.value < listModel.count) {
            listModel.remove(listModel.count - 1)
            lastX -= 42
            lastY += 2
            lastIndex--

            valuesOfXs.pop()
        }
    }

    Component.onCompleted:  {
        var temp = []
        spinBox.value = 1
        valuesOfXs = temp
        creator()
    }



    IntValidator {
        id: intvalidator
        bottom: 0
        top: 10000
    }

    ListModel {
        id: listModel
    }


    addBtn.onClicked: {
        var list = []

        list.push(parseInt(index.text))

        for(var i = 0; i < listModel.count; i++) {

            list.push(valuesOfXs[i])
        }

        CoreApi.addEquation(parseInt(index.text), list)
        added()

        listModel.clear()
        lastIndex = 0
        lastX = 0
        lastY = 0

        spinBox.value = 1

        creator()

    }

    removeBtn.onClicked:  {
        CoreApi.removeEquation(parseInt(index.text))
    }



    Component {
        id: delegateItem
        Item {
            property string val: tf.text

            TextField {
                id: tf
                validator: intvalidator
                x: lx
                y: ly

                text: "0"

                height: 40
                width: 40

                onTextChanged:  {
                    valuesOfXs[li] = parseInt(text)
                }

            }
        }
    }
}
